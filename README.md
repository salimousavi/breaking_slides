# Slides

<h3> Installation </h3>

Download this module and add in module directory and enable it.

<h3> How to work? </h3>
This module create a vocabulary that named <code>slides</code>.<br>

<b>Fields:</b>
 <br><code> field_slide_enable </code>
 <br><code> field_slide </code>

<b>Route request:</b>
 <br><code> api/slides/tiny </code> For Tiny slides
 <br><code> api/slides/big </code> For Big slides

<b>Response:</b>
 <br><code>name</code>  Example: Lorem ipsum
 <br><code>src</code> Example: Source of image
 
 
