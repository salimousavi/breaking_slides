<?php

namespace Drupal\breaking_slides\Controller;

use Drupal\image\Entity\ImageStyle;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Created by PhpStorm.
 * User: kasper
 * Date: 1/31/17
 * Time: 11:11 AM
 */
class BreakingSlidesController {

    public function index() {

        $db = \Drupal::database();
        $result = $db->select('breaking_slides', 'e')->fields('e')->orderBy('id', 'DESC')->execute()->fetchObject();

        if ($result) {

            $time = $result->time;
            $duration = $result->duration;

            if (time() - $time < $duration) {

                $style = ImageStyle::load('big_slide');
                $terms = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadByProperties(['vid' => 'breaking_slides']);

                usort($terms, function ($a, $b) {
                    if ($a->getWeight() == $b->getWeight()) {
                        return 0;
                    }

                    return ($a->getWeight() > $b->getWeight()) ? -1 : 1;
                });

                $r = [];
                foreach ($terms as $term) {

                    $r[] = [
                        'name' => $term->getName(),
                        'src'  => file_create_url($style->buildUrl($term->field_breaking_slide->entity->uri->value)),
                    ];
                }

                return new JsonResponse($r);
            }
        }


        return new JsonResponse([]);
    }

}