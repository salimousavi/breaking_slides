<?php

namespace Drupal\breaking_slides\Form;


use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Implements the RefreshingForm form controller.
 *
 *
 * @see \Drupal\Core\Form\FormBase
 */
class BreakingSlidesForm extends FormBase {

    /**
     * Build the BreakingSlides form.
     *
     * A build form method constructs an array that defines how markup and
     * other form elements are included in an HTML form.
     *
     * @param array              $form
     *   Default form array structure.
     * @param FormStateInterface $form_state
     *   Object containing current form state.
     *
     * @return array
     *   The render array defining the elements of the form.
     */
    public function buildForm(array $form, FormStateInterface $form_state) {

        $form['title'] = [
            '#type'     => 'textfield',
            '#title'    => $this->t('Why enable breaking slides?'),
            '#required' => TRUE,
        ];

        $form['duration'] = [
            '#type'     => 'textfield',
            '#title'    => $this->t('Duration time show slides(enter in seconds and latin mode)'),
            '#required' => TRUE,
        ];

        // Group submit handlers in an actions element with a key of "actions" so
        // that it gets styled correctly, and so that other modules may add actions
        // to the form. This is not required, but is convention.
        $form['actions'] = [
            '#type' => 'actions',
        ];

        // Add a submit button that handles the submission of the form.
        $form['actions']['submit'] = [
            '#type'  => 'submit',
            '#value' => $this->t('Enable'),
        ];

        $form['actions']['cancle'] = array
        (
            '#type' => 'submit',
            '#value' => t('Cancel all breaking slides'),
            '#submit' => array('::delete_all_breaking_slides_record'),
        );
        return $form;
    }

    /**
     * Getter method for Form ID.
     *
     * The form ID is used in implementations of hook_form_alter() to allow other
     * modules to alter the render array built by this form controller.  it must
     * be unique site wide. It normally starts with the providing module's name.
     *
     * @return string
     *   The unique ID of the form defined by this class.
     */
    public function getFormId() {
        return 'entekhab_breaking_slides_form';
    }

    /**
     * Implements form validation.
     *
     * The validateForm method is the default method called to validate input on
     * a form.
     *
     * @param array              $form
     *   The render array of the currently built form.
     * @param FormStateInterface $form_state
     *   Object describing the current state of the form.
     */
    public function validateForm(array &$form, FormStateInterface $form_state) {
        $title = $form_state->getValue('title');
        if (!strlen($title)) {
            // Set an error for the form element with a key of "title".
            $form_state->setErrorByName('title', $this->t('The title is required.'));
        }
        $duration = $form_state->getValue('duration');
        if (!strlen($duration)) {
            // Set an error for the form element with a key of "title".
            $form_state->setErrorByName('duration', $this->t('The duration time is required'));
        }
    }

    /**
     * Implements a form submit handler.
     *
     * The submitForm method is the default method called for any submit elements.
     *
     * @param array              $form
     *   The render array of the currently built form.
     * @param FormStateInterface $form_state
     *   Object describing the current state of the form.
     */
    public function submitForm(array &$form, FormStateInterface $form_state) {
        $title = $form_state->getValue('title');
        $duration = $form_state->getValue('duration');
        $user = $this->currentUser();

        $db = \Drupal::database();
        $db->insert('breaking_slides')->fields(array(
            'title'    => $title,
            'duration' => $duration,
            'time'     => REQUEST_TIME,
            'uid'      => $user->id(),
        ))->execute();

        drupal_set_message(t('The breaking slide enabled because %title.', ['%title' => $title]));
    }

    public function delete_all_breaking_slides_record(array &$form, FormStateInterface $form_state) {
        $db = \Drupal::database();
        $db->delete('breaking_slides')->execute();
        drupal_set_message(t('The breaking slides disabled successfully.'));
    }
}
